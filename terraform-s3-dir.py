# -*- coding: utf-8 -*-
import argparse
import os
import re
import hashlib
import mimetypes
import json


def md5(name):
    m = hashlib.md5()
    m.update(name.encode())
    return m.hexdigest()

def get_mime_type(file):
    filename, file_extension = os.path.splitext(file)
    try:
        return mimetypes.types_map[file_extension]
    except Exception:
        return 'application/octet-stream'


parser = argparse.ArgumentParser()
parser.add_argument('-d', '--dir', action='store', help='root dir', required=True)
parser.add_argument('-b', '--bucket', action='store', help='bucket name', required=True)

mimetypes.init()

args = parser.parse_args()

res = {}
for root, subdirs, files in os.walk(args.dir):
    for filename in files:
        file_path = os.path.join(root, filename)
        res.update(
            {
                md5(file_path):{
                    'bucket': args.bucket,
                    'content_type': get_mime_type(file_path),
                    'etag': '${md5(file("%s"))}' % file_path,
                    'key': re.sub(args.dir, '', file_path).lstrip('/'),
                    "source": file_path,
                }
            }
        )

print(json.dumps({"resource":{"aws_s3_bucket_object": res}}))
