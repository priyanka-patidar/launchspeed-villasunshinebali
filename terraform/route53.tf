resource "aws_route53_record" "site" {
  zone_id = "${var.zone_id}"
  name    = "${var.host_name}"
  type    = "A"

  alias {
    name                   = "${module.cloudfront.domain_name}"
    zone_id                = "${module.cloudfront.hosted_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www-site" {
  zone_id = "${var.zone_id}"
  name    = "www"
  type    = "CNAME"
  ttl     = "3600"
  records = ["${aws_s3_bucket.site_www.website_endpoint}"]
}
