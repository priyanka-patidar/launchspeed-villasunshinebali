module "cloudfront" {
  source = "modules/cloud_front"

  origin_domain_name = "${var.host_name}.s3.amazonaws.com"
  origin_id          = "s3-bucket-${var.host_name}"

  certificate_acm_arn = "${module.us-east-1-acm.arn}"

  aliases = [
    "${var.host_name}",
    "www.${var.host_name}",
  ]

  tags {
    "Terraform"   = "true"
    "ProjectName" = "${var.project_name}"
  }
}
