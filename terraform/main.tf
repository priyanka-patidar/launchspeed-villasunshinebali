resource "aws_s3_bucket" "site" {
  bucket        = "${var.host_name}"
  acl           = "public-read"
  force_destroy = true

  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[{
      "Sid": "PublicReadGetObject",
    "Effect": "Allow",
    "Principal": "*",
    "Action":["s3:GetObject"],
    "Resource":[
      "arn:aws:s3:::${var.host_name}/*"
    ]
  }]
}
EOF

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags {
    ProjectName = "${var.project_name}"
    HostName    = "${var.host_name}"
  }
}

resource "aws_s3_bucket" "site_www" {
  bucket        = "www.${var.host_name}"
  acl           = "public-read"
  force_destroy = true

  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[{
      "Sid": "PublicReadGetObject",
    "Effect": "Allow",
    "Principal": "*",
    "Action":["s3:GetObject"],
    "Resource":[
      "arn:aws:s3:::www.${var.host_name}/*"
    ]
  }]
}
EOF

  website {
    redirect_all_requests_to = "${var.host_name}"
  }

  tags {
    ProjectName = "${var.project_name}"
    HostName    = "www.${var.host_name}"
  }
}
