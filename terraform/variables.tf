variable "aws_region" {
  default = "ap-southeast-1"
}

variable "host_name" {
  default = "example.com"
}

variable "project_name" {
  default = "LaunchSpeed"
}

variable "zone_id" {}
