module "us-east-1-acm" {
  source   = "modules/data/aws_acm_certificate"
  region   = "us-east-1"
  domain   = "${var.host_name}"
  statuses = ["ISSUED"]
}
