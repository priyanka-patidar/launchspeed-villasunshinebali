variable "region" {
  default = "us-west-2"
}

variable "domain" {}

variable "statuses" {
  default = ["ISSUED"]
}
