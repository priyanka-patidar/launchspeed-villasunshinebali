output "arn" {
  value = "${data.aws_acm_certificate.mod.arn}"
}

output "domain" {
  value = "${data.aws_acm_certificate.mod.domain}"
}
