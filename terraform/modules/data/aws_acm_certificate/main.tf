provider "aws" {
  region = "${var.region}"
  alias  = "mod"
}

data "aws_acm_certificate" "mod" {
  domain   = "${var.domain}"
  statuses = "${var.statuses}"
  provider = "aws.mod"
}
