output "domain_name" {
  value = "${aws_cloudfront_distribution.mod.domain_name}"
}

output "hosted_zone_id" {
  value = "${aws_cloudfront_distribution.mod.hosted_zone_id}"
}
