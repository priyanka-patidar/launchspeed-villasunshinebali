variable "origin_domain_name" {}
variable "origin_id" {}

variable "is_ipv6_enabled" {
  default = true
}

variable "comment" {
  default = "Managed by Terraform"
}

variable "default_root_object" {
  default = "index.html"
}

#variable "logging_config_bucket" {}
#variable "logging_config_prefix" {}

variable "aliases" {
  default = []
}

variable "certificate_acm_arn" {}

variable "tags" {
  default = {}
}
