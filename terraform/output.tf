output "address" {
  value = "${aws_s3_bucket.site.website_endpoint}"
}

output "www_address" {
  value = "${aws_s3_bucket.site_www.website_endpoint}"
}
